#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QPainterPath>
#include <cmath>
#include <algorithm>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    plotScene = new(QGraphicsScene);
    ui->graphicsViewPlotArea->setScene(plotScene);
    plotScene->setSceneRect(ui->graphicsViewPlotArea->rect());
}

MainWindow::~MainWindow()
{
    delete ui;
    delete plotScene;
}

void MainWindow::LoadFile()
{
    QString fileName = QFileDialog::getOpenFileName( this );
    if (!fileName.isEmpty())
    {
        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            ShowMessage("File not open");
        }
        else
        {
            ClearAll();

            ui->textBrowserData->append(fileName);
            QTextStream dataStream(&file);
            QString tmp = dataStream.readLine();
            while ('#' == tmp[0])
            {
                ui->textBrowserData->append(tmp);
                tmp = dataStream.readLine();
            }

            bool flag = false;
            auto pair = ParseString(tmp, &flag );
            if (flag)
            {
                rawData.push_back(pair);

                while ( !dataStream.atEnd())
                {
                    tmp = dataStream.readLine();
                    if (tmp.isEmpty())
                    {
                        continue;
                    }
                    pair = ParseString(tmp, &flag );
                    if (flag)
                    {
                        rawData.push_back(pair);
                    }
                    else
                    {
                        ShowMessage("File Corrupted, " + QString::number(rawData.size()) + " Points Readed");
                        break;
                    }
                }

                auto tmpMinMax = std::minmax_element(rawData.begin(), rawData.end(),
                                                [](const QPair<double, double> & lhs, const QPair<double, double> & rhs)
                                                {
                                                    return lhs.second < rhs.second;
                                                });
                maxY = (*tmpMinMax.second).second;
                minY = (*tmpMinMax.first).second;

                minX = rawData[0].first;
                maxX = rawData.back().first;

                PlotGraph();

            }
            else
            {
                ShowMessage("Wrong File Format");
            }
        }
    }


}


void MainWindow::on_pushButtonBrowse_clicked()
{
    LoadFile();
}

void MainWindow::ShowMessage( const QString & message)
{
    QMessageBox errorBox;
    errorBox.setText(message);
    errorBox.exec();
}

void MainWindow::PlotGraph()
{
    QMap<int, QVector<double>> tmp = {};

    tmp[PrepareX(rawData.front().first)].push_back(rawData.front().second);

    for ( auto it = rawData.cbegin(); it != rawData.cend(); ++ it)
    {
        auto rawPoint = *it;
        tmp[PrepareX(rawPoint.first)].push_back(rawPoint.second);
    }




    auto it = tmp.constKeyValueBegin();
    auto data = (*it).second;
    auto key = (*it).first;

    QPainterPath path(QPoint(key, PrepareY(data.front())));

    for ( auto it = tmp.constKeyValueBegin(); it != tmp.constKeyValueEnd(); ++ it)
    {
        auto data = (*it).second;
        auto key = (*it).first;
        QPoint beginPoint(key, PrepareY(data.front()));
        QPoint endPoint(key, PrepareY(data.back()));

        auto rawMinmax = std::minmax_element(data.begin(), data.end());
        QPoint minPoint(key, PrepareY(*(rawMinmax.first)));
        QPoint maxPoint(key, PrepareY(*(rawMinmax.second)));

        path.lineTo(beginPoint);
        path.moveTo(minPoint);
        path.lineTo(maxPoint);
        path.moveTo(endPoint);
    }





    plotScene->addPath(path);
}

int MainWindow::PrepareCoordinate( double val, double minVal, double maxVal, double width)
{
    return round( width * (val - minVal) / (maxVal - minVal));
}

int MainWindow::PrepareX( double val)
{
    return PrepareCoordinate( val, minX, maxX, plotScene->width());
}

int MainWindow::PrepareY( double val)
{
    return plotScene->height() - PrepareCoordinate( val, minY, maxY, plotScene->height());
}

void MainWindow::ClearAll()
{
    ui->textBrowserData->clear();
    ui->graphicsViewPlotArea->scene()->clear();
    rawData.erase(rawData.begin(),rawData.end());
}

QPair<double, double> MainWindow::ParseString(const QString & s, bool *flag )
{
    QPair<double, double> result = {0,0};
    auto parts = s.split(' ', Qt::SkipEmptyParts);
    *flag = false;
    if (2 == parts.size())
    {
        bool timestampOk = false;
        result.first = parts[0].toDouble(&timestampOk);
        bool dataOk = false;
        result.second = parts[1].toDouble(&dataOk);
        *flag = timestampOk && dataOk;
    }
    return result;
}
