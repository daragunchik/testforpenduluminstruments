#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButtonBrowse_clicked();

private:
    QVector<QPair<double, double>> rawData = {};
    Ui::MainWindow *ui;
    QGraphicsScene *plotScene;
    void LoadFile();
    void ShowMessage(const QString &message);
    void PlotGraph();
    double maxX = 0;
    double maxY = 0;
    double minX = 0;
    double minY = 0;
    int PrepareCoordinate(double val, double minVal, double maxVal, double width);
    int PrepareY(double val);
    int PrepareX(double val);
    void ClearAll();
    QPair<double, double> ParseString(const QString &s, bool *flag = nullptr);
};
#endif // MAINWINDOW_H
